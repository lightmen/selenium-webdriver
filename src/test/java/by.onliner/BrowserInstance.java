package by.onliner;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class BrowserInstance {

    private static WebDriver driver;

    private enum  BrowserType {
        FireFox,
        Chrome,
        IE
    }

    public static WebDriver getBrowserInstance (String browser) {
        synchronized (BrowserInstance.class) {
            getBrowserDriver(browser);
        }

        return driver;
    }

    private static void  getBrowserDriver (String browser) {
        switch (BrowserType.valueOf(browser)) {
            case FireFox:
                getGeckoDriver();
                break;
            case Chrome :
                getChromeDriver();
                break;
            case IE:
                getInternetExplorerDriver();
                break;
        }
    }

    private static void getGeckoDriver(){
        System.setProperty("webdriver.gecko.driver", ".\\geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    private static void getChromeDriver (){
        System.setProperty("webdriver.chrome.driver", ".\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    private static void getInternetExplorerDriver (){
        System.setProperty("webdriver.ie.driver",".\\IEDriverServer.exe");
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

}

