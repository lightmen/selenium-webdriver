package by.onliner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;


@RunWith(Parameterized.class)
public class FirstTest extends BrowserInstance {

    @Parameterized.Parameter()
    public String browser;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{ {"FireFox"}, {"Chrome"}, {"IE"}};
        return Arrays.asList(data);
    }

    private WebDriver driver;

    @Before
    public void init(){
        driver = getBrowserInstance(browser);
    }


    @Test
    public void firstTest() throws InterruptedException {

        driver.get("http://onliner.by/");
        WebElement enterButton = driver.findElement(By.className("auth-bar__item--text"));
        enterButton.click();

        // login form

        WebElement login = driver.findElement(By.xpath("//input[@placeholder='Ник или e-mail']"));
        login.sendKeys("lightmen09@gmail.com");

        WebElement password = driver.findElement(By.xpath("//input[@placeholder='Пароль']"));
        password.sendKeys("Pasword01");

        enterButton = driver.findElement(By.className("auth-form__button_width_full"));
        enterButton.click();

        //Thread.sleep(2500);

        // search catalog
        WebDriverWait wait = new WebDriverWait(driver, 100);
        WebElement catalogLink = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@class='b-main-navigation']/li[1]/a")));
        catalogLink.click();

        List<WebElement> links = driver.findElements(By.className("catalog-bar__item"));
        List<WebElement> visibleLinks = new ArrayList<WebElement>();
        for (WebElement link : links){
            if(link.isDisplayed())
            visibleLinks.add(link);
        }
        Random r = new Random();
        WebElement randomElement=visibleLinks.get(r.nextInt(visibleLinks.size()));
        String randomElementText = randomElement.getText();

        randomElement.click();

        WebElement catalogCategoryTitle = wait.until(ExpectedConditions.elementToBeClickable(By.tagName("h1")));
        String catalogCategoryTitleText = catalogCategoryTitle.getText();

        Assert.assertTrue(randomElementText.equals(catalogCategoryTitleText));

        WebElement togglebar = driver.findElement(By.xpath("//a[@class='b-top-profile__preview js-toggle-bar']"));
        togglebar.click();

        WebElement logout = driver.findElement(By.xpath("//a[contains(text(),'Выйти')]"));
        logout.click();

    }


    @After
    public void close () {
       driver.quit();
    }

}
